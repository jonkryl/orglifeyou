package ru.orglifeyou.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import ru.orglifeyou.R;

public class AddTaskDialog extends DialogFragment {
    private EditText inputFieldAddTask;
    private ImageView okButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_task_dialog, null);
        initSource(v);
        return v;
    }

    protected void initSource(View v){
        inputFieldAddTask = v.findViewById(R.id.input_field_add_task);
        okButton = v.findViewById(R.id.ok_button);
        inputFieldAddTask.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (inputFieldAddTask.getText().toString().equals("")){
                    okButton.setVisibility(View.GONE);
                }else {
                    okButton.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }
}

