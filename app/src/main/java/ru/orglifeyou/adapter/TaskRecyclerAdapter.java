package ru.orglifeyou.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.orglifeyou.R;
import ru.orglifeyou.model.Task;

public class TaskRecyclerAdapter extends RecyclerView.Adapter<TaskRecyclerAdapter.TaskViewHolder> {
    private ArrayList<Task> tasks;
    private int reversePosition;
    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task_layout, parent, false);
        TaskViewHolder taskViewHolder = new TaskViewHolder(v);
        return taskViewHolder;
    }

    public TaskRecyclerAdapter() {
        tasks = new ArrayList<>();
    }

    public void addTask(Task task){
        tasks.add(task);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        reversePosition = tasks.size() - position - 1;
        holder.taskName.setText(tasks.get(reversePosition).getTaskName());
        holder.projectName.setText(tasks.get(reversePosition).getProjectName());
        holder.executorName.setText(tasks.get(reversePosition).getExecutorName());
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    public class TaskViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView executorAvatar;
        private TextView taskName;
        private TextView executorName;
        private TextView projectName;
        private TextView date;

        public TaskViewHolder(View itemView) {
            super(itemView);
            executorAvatar = itemView.findViewById(R.id.user_avatar);
            taskName = itemView.findViewById(R.id.task_name);
            executorName = itemView.findViewById(R.id.executor_name);
            projectName = itemView.findViewById(R.id.project_name);
            date = itemView.findViewById(R.id.date);
        }
    }
}
