package ru.orglifeyou;

import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import ru.orglifeyou.adapter.FragmentPagerAdapter;
import ru.orglifeyou.fragment.ImportantFragment;
import ru.orglifeyou.fragment.ProjectFragment;
import ru.orglifeyou.fragment.UrgentFragment;

public class MainActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private FragmentPagerAdapter fragmentPagerAdapter;
    private UrgentFragment urgentFragment;
    private ImportantFragment importantFragment;
    private ProjectFragment projectFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setElevation(0);
        initSwipe();
    }
    protected void initSwipe(){
        urgentFragment = new UrgentFragment();
        importantFragment = new ImportantFragment();
        projectFragment = new ProjectFragment();
        tabLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.viewpager);
        fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager());
        fragmentPagerAdapter.addFragment(urgentFragment);
        fragmentPagerAdapter.addFragment(importantFragment);
        fragmentPagerAdapter.addFragment(projectFragment);
        viewPager.setAdapter(fragmentPagerAdapter);
        final TabLayout.Tab urgent = tabLayout.newTab();
        final TabLayout.Tab important = tabLayout.newTab();
        final TabLayout.Tab project = tabLayout.newTab();

        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.main_text_app_color));
        urgent.setText(R.string.urgent_text);
        important.setText(R.string.important_text);
        project.setText(R.string.project_text);

        tabLayout.addTab(urgent, 0);
        tabLayout.addTab(important, 1);
        tabLayout.addTab(project, 2);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            hideSoftKeyboard();

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    private void hideSoftKeyboard() {
        if(getCurrentFocus()!= null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
