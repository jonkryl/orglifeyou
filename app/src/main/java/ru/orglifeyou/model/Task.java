package ru.orglifeyou.model;

public class Task {
    private String taskName;
    private String executorName;
    private long date;
    private boolean isSucceful;
    private boolean isOverdue;
    private String projectName;

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getExecutorName() {
        return executorName;
    }

    public void setExecutorName(String executorName) {
        this.executorName = executorName;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public boolean isSucceful() {
        return isSucceful;
    }

    public void setSucceful(boolean succeful) {
        isSucceful = succeful;
    }

    public boolean isOverdue() {
        return isOverdue;
    }

    public void setOverdue(boolean overdue) {
        isOverdue = overdue;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
