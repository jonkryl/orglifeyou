package ru.orglifeyou.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;

import ru.orglifeyou.R;
import ru.orglifeyou.adapter.TaskRecyclerAdapter;
import ru.orglifeyou.dialog.AddTaskDialog;
import ru.orglifeyou.model.Task;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class UrgentFragment extends Fragment {
    private FloatingActionButton addTask;
    private AddTaskDialog addTaskDialog;
    private LinearLayout inputTaskContainer;
    private EditText inputFieldAddTask;
    private ImageView okButton;
    private ImageView closeButton;
    private boolean isFirstSymbol;
    private ArrayList<Task> tasks;
    private TaskRecyclerAdapter taskRecyclerAdapter;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView todayRecyclerView;
    private RecyclerView tomorrowRecyclerView;
    private RecyclerView nonUrgent;
    private RecyclerView otherDay;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.urgent_fragment, container, false);
        initSource(v);
        return v;
    }

    {
        tasks = new ArrayList<>();
    }

    private void initSource(View v) {
        taskRecyclerAdapter = new TaskRecyclerAdapter();
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setStackFromEnd(true);
        todayRecyclerView = v.findViewById(R.id.todayRecyclerView);
        todayRecyclerView.setAdapter(taskRecyclerAdapter);
        todayRecyclerView.setLayoutManager(linearLayoutManager);
        addTask = v.findViewById(R.id.addTask);
        addTaskDialog = new AddTaskDialog();
        inputTaskContainer = v.findViewById(R.id.input_task_container);
        inputFieldAddTask = v.findViewById(R.id.input_field_add_task);
        okButton = v.findViewById(R.id.ok_button);
        closeButton = v.findViewById(R.id.close_button);
        addTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputTaskContainer.setVisibility(View.VISIBLE);
                inputFieldAddTask.requestFocus();
                addTaskPanelAnimation(inputTaskContainer);
                showKeyboard(inputFieldAddTask);
                addTask.setVisibility(View.GONE);
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputTaskContainer.setVisibility(View.GONE);
                hideSoftKeyboard(inputFieldAddTask);
                inputFieldAddTask.setText("");
                inputTaskContainer.setAnimation(null);
                isFirstSymbol = false;
                addTask.setVisibility(View.VISIBLE);
            }
        });
        inputFieldAddTask.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (inputFieldAddTask.getText().toString().equals("")) {
                    okButton.setVisibility(View.GONE);
                    okButton.setAnimation(null);
                    isFirstSymbol = false;
                } else {
                    okButton.setVisibility(View.VISIBLE);
                    if (!isFirstSymbol) {
                        okButtonAnimation(okButton);
                        isFirstSymbol = true;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTask(inputFieldAddTask.getText().toString());
                inputFieldAddTask.setText("");
                inputTaskContainer.setAnimation(null);
                inputTaskContainer.setVisibility(View.GONE);
                addTask.setVisibility(View.VISIBLE);
                hideSoftKeyboard(inputFieldAddTask);

            }
        });

    }

    protected void showKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    private void hideSoftKeyboard(View v) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
    protected void addTaskPanelAnimation(View view){
        TranslateAnimation translateAnimation = new TranslateAnimation(-400, 0, 0, 0);
        translateAnimation.setDuration(1000);
        translateAnimation.setFillAfter(true);
        view.setAnimation(translateAnimation);
    }
    protected void okButtonAnimation(View view){
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.8f, 1.0f);
        scaleAnimation.setDuration(300);
        view.setAnimation(scaleAnimation);
    }
    protected void addTask(String taskName){
        Task task = new Task();
        task.setTaskName(taskName);
        taskRecyclerAdapter.addTask(task);
        taskRecyclerAdapter.notifyDataSetChanged();

    }
}